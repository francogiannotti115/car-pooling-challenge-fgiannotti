#
# Build stage
#
FROM golang:1.15-alpine3.12 as compiler
WORKDIR /go/src/gitlab.com/fgiannotti/car-pooling-challenge-fgiannotti
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -v -ldflags "-extldflags '-static'"
#
# Run stage
#
FROM alpine
COPY --from=compiler /go/src /go/src
RUN apk add --no-cache tzdata
WORKDIR /go/src/gitlab.com/fgiannotti/car-pooling-challenge-fgiannotti
ENV PORT=9091
CMD ./car-pooling-challenge-fgiannotti
EXPOSE 9091