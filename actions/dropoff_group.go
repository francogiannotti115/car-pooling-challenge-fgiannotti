package actions

import (
	"context"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/dto"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/cars"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/groups"
	"go.uber.org/zap"
	"strconv"
)

//go:generate mockgen -destination=./mock_dropoff_group/mock_dropoff_group.go -source=dropoff_group.go
type DropoffGroup interface {
	Invoke(ctx context.Context, request dto.DropoffRequest) error
}

type DropoffGroupService struct {
	groupsRepo groups.Repo
	carsRepo   cars.Repo
	logger     *zap.SugaredLogger
}

func NewDropoffGroupService(groupRepo groups.Repo, carsRepo cars.Repo, logger *zap.SugaredLogger) *DropoffGroupService {
	return &DropoffGroupService{groupsRepo: groupRepo, carsRepo: carsRepo, logger: logger}
}

func (u *DropoffGroupService) Invoke(ctx context.Context, request dto.DropoffRequest) error {
	groupID, err := strconv.Atoi(request.ID)
	if err != nil {
		return err
	}
	group, err := u.groupsRepo.Get(groupID)
	if err != nil {
		u.logger.Error("Error getting group: " + err.Error())
		return err
	}
	if group.Car != nil {
		err = u.carsRepo.ReleaseSeats(*group.Car)
		if err != nil {
			u.logger.Error("Error releasing car: " + err.Error())
			return err
		}
		group.Car = nil
	}
	err = u.groupsRepo.Delete(groupID)
	if err != nil {
		u.logger.Error("Error deleting group " + request.ID + ": " + err.Error())
		return err
	}
	return nil
}
