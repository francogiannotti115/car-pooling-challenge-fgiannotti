package actions

import (
	"context"
	"errors"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/dto"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/groups"
	"go.uber.org/zap"
)

//go:generate mockgen -destination=./mock_locate_group/mock_locate_group.go -source=locate_group.go
type LocateGroup interface {
	Invoke(ctx context.Context, request dto.LocateRequest) (domain.Group, error)
}

type LocateGroupService struct {
	groupsRepo groups.Repo
	logger     *zap.SugaredLogger
}

func NewLocateGroupService(groupRepo groups.Repo, logger *zap.SugaredLogger) *LocateGroupService {
	return &LocateGroupService{groupsRepo: groupRepo, logger: logger}
}

func (u *LocateGroupService) Invoke(ctx context.Context, request dto.LocateRequest) (domain.Group, error) {
	group, err := u.groupsRepo.Get(request.ID)
	if err != nil {
		if !errors.Is(err, groups.GroupNotFoundError) {
			return domain.Group{},nil
		}
	}
	return group, err
}