package actions

import (
	"context"
	"errors"
	"fmt"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/dto"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/cars"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/groups"
	"go.uber.org/zap"
	"math"
)

//go:generate mockgen -destination=./mock_post_journey/mock_post_journey.go -source=post_journey.go
type PostJourney interface {
	Invoke(ctx context.Context, request dto.JourneyRequest) error
}

type PostJourneyService struct {
	carsRepo   cars.Repo
	groupsRepo groups.Repo
	logger     *zap.SugaredLogger
}

func NewPostJourneyService(carsRepo cars.Repo, groupsRepo groups.Repo, logger *zap.SugaredLogger) *PostJourneyService {
	return &PostJourneyService{carsRepo: carsRepo, groupsRepo: groupsRepo, logger: logger}
}

// Invoke : Given a Group, it should assign it the equal matching space car
//if there is none, it should try with the next one in space.
//(i.e group with 4 but there is no car with 4 seats, it should try with a car with 5 seats)
//Then update the remaining seats of that car.

func (u *PostJourneyService) Invoke(ctx context.Context, request dto.JourneyRequest) error {
	group, err := u.groupsRepo.Get(request.ID)
	if err != nil {
		if !errors.Is(err, groups.GroupNotFoundError) { //ignore group not found error
			u.logger.Error("Error getting group from DB")
			return err
		}
	}
	if group.Car != nil {
		u.logger.Error(groups.GroupAlreadyAssigned.Error())
		return fmt.Errorf("%w: groupID %d", groups.GroupAlreadyAssigned, group.ID)
	}

	carsBySeats, err := u.carsRepo.GetAllByRemainingSeats()
	if err != nil {
		u.logger.Error("Error getting cars from DB")
		return err
	}
	groupQuantity := request.People
	newGroup := domain.Group{ID: request.ID, PeopleAmount: groupQuantity, Car: nil}

	for i := groupQuantity; i <= cars.MaxCarSeats; i++ {
		existsCar := len(carsBySeats[i]) > 0
		if existsCar {
			assignedCar, _ := u.carsRepo.GetAnyWithRemainingSeats(i)

			//if there is still space, add the car to the repo
			assignedCar.RemainingSeats -= int(math.Max(float64(groupQuantity), float64(0)))
			err = u.carsRepo.Update(assignedCar)
			if err != nil {
				u.logger.Error("Error updating Car")
				return err
			}

			newGroup.Car = &assignedCar
			break
		}
	}

	return u.groupsRepo.Save(newGroup)
}
