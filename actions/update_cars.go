package actions

import (
	"context"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/dto"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/cars"
	"go.uber.org/zap"
)

//go:generate mockgen -destination=./mock_update_cars/mock_update_cars.go -source=update_cars.go
type UpdateCars interface {
	Invoke(ctx context.Context, request dto.PutCarsRequest) error
}

type UpdateCarsService struct {
	carsRepo cars.Repo
	logger   *zap.SugaredLogger
}

func NewUpdateCarsService(repo cars.Repo, logger *zap.SugaredLogger) *UpdateCarsService {
	return &UpdateCarsService{carsRepo: repo, logger: logger}
}

func (u *UpdateCarsService) Invoke(ctx context.Context, request dto.PutCarsRequest) error {
	err := u.carsRepo.Flush()
	if err != nil {
		u.logger.Info("Error flushing DB")
		return err
	}
	//TODO: release all groups's cars and try to assign each car thats being saved

	for _, car := range toDomainCars(request.Cars) {
		err = u.carsRepo.Save(car)
		if err != nil {
			u.logger.Info("Error saving car")
			return err
		}
	}
	return nil
}

func toDomainCars(cars []dto.CarDTO) []domain.Car {
	result := make([]domain.Car, 0, len(cars))
	for _, car := range cars {
		result = append(result, car.ToDomainCar())
	}
	return result
}
