package actions

import (
	"context"
	"errors"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/dto"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/cars/mock_cars"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zaptest"
	"testing"
)

var testCarRequest = dto.PutCarsRequest{Cars: []dto.CarDTO{{ID: 1, TotalSeats: 6}}}
var FatalError = errors.New("It's over 9000")

func TestUpdateCarsExecutesSuccessfully(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	carsRepoMock := mock_cars.NewMockRepo(ctrl)
	loggerMock := zaptest.NewLogger(t).Sugar()

	carsRepoMock.EXPECT().Flush().Return(nil).Times(1)
	carsRepoMock.EXPECT().Save(gomock.Any()).Return(nil).Times(1)

	err := NewUpdateCarsService(carsRepoMock, loggerMock).Invoke(context.Background(), testCarRequest)

	assert.NoError(t, err, "No error expected")
}

func TestUpdateCarsReturnsErrorFlushFails(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	carsRepoMock := mock_cars.NewMockRepo(ctrl)
	loggerMock := zaptest.NewLogger(t).Sugar()

	carsRepoMock.EXPECT().Flush().Return(FatalError).Times(1)

	err := NewUpdateCarsService(carsRepoMock, loggerMock).Invoke(context.Background(), testCarRequest)

	assert.Error(t, err, "error expected")
}

func TestUpdateCarsReturnsErrorBulkSaveFails(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	carsRepoMock := mock_cars.NewMockRepo(ctrl)
	loggerMock := zaptest.NewLogger(t).Sugar()

	carsRepoMock.EXPECT().Flush().Return(nil).Times(1)
	carsRepoMock.EXPECT().Save(gomock.Any()).Return(FatalError).Times(1)

	err := NewUpdateCarsService(carsRepoMock, loggerMock).Invoke(context.Background(), testCarRequest)

	assert.Error(t, err, "error expected")
}
