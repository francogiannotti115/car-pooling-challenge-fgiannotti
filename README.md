# Car Pooling Service Challenge 🚗

# Franco Giannotti Notes
#### Total hours consumed: 12h

We are being asked to design/implement a system to manage car pooling.

I'm going to use this readme to write down my thought process and any assumptions made.

First of all, the exercise asks us: **_to optimize the use of resources by introducing car pooling_**. Which means we
have to use the minimum amount of cars for a given set of groups.


----------
**Assumption 1:** Cars can be assigned any amount of groups at any time, if they have enough space.

----------

### **About Journeys** :briefcase:

Since the groups are going to request journeys one by one, this could lead to suboptimal car management. To improve
that, we can put the pending journeys in a queue and take care of them asynchronously. (Returning an _Accepted 202_ HTTP
code)

This asynchronous process should:

- Dispatch groups (which comes in a given order) to the most optimal car.
- Run after X time. (TBD validate the best time. it's a tradeoff between car optimization and accepted delay for the
  user)

#### **Dispatching Algorithm** :open_file_folder:

The process of dispatching cars consist of an input like this:
G groups and C cars
1. Execute a counting sort on C. This allow us to optimize the cars space to a 6 index array
   - i.e. if `C[i].seats` is [5,4,6,6,6,6] -> [0,0,0,1,1,4] 
2. Try to index a Car of equal size to the amount of people in that group (this should be looped through the 6 index array)
3. If it matches, use that car and update its remaining seats.
4. If it doesn't, go to step 2 but with index increased by one.
5. If there is no 
----------

### **About Dropoffs** :arrow_down:

This is another process that modifies the state of the cars. Which should at least do:

- Unassign the car assigned to that group
- Update the remaining seats of that car.
----------

### **About Car Updating**

This process will flush the DB and save the cars. 

1 million cars takes about 1500ms to be saved, and could be reduced by preallocating memory for them to 1 second.

After flushing the DB, all groups lose their groups, I launch a process to re-assign them

----------
### **Concurrency problem**

The Journeys, Dropoffs, and also the loading of new cars (PUT /cars) modifies our repository of Cars. 

Because any of these can happen at the same time, we need to lock it so that it can only be sequential. 

This would avoid very difficult to catch problems like 2 groups assigned to the same Car.

-----------
### **About project structure**

I used some kind of DDD where I have: 
- A delivery layer (controllers + dtos)
- An actions layer, which are my use cases for the service and also the entrypoint to my domain.
- A domain layer, which is compose by my models and interfaces to obtain the data needed
