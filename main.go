package main

import (
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/actions"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/cmd"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/controllers"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/controllers/groups"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/cars/memory_cars"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/groups/memory_groups"
	"go.uber.org/zap"
	"os"
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}

	defer logger.Sync()

	sugar := logger.Sugar()
	sugar.Info("Running go server...")

	carsRepo := memory_cars.NewMemoryCarsRepo(sugar)
	groupsRepo := memory_groups.NewMemoryGroupsRepo(sugar)

	carsController := controllers.NewCarsController(sugar, actions.NewUpdateCarsService(carsRepo, sugar))
	groupsController := groups.NewGroupsController(
		sugar,
		actions.NewPostJourneyService(carsRepo, groupsRepo, sugar),
		actions.NewLocateGroupService(groupsRepo, sugar),
		actions.NewDropoffGroupService(groupsRepo, carsRepo, sugar),
	)

	r := cmd.SetupRouter(carsController, groupsController)
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	r.Run(":"+port)
}
