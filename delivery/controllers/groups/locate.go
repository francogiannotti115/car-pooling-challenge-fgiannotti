package groups

import (
	"errors"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/dto"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/groups"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func (controller *GroupsController) HandleLocate(ctx *gin.Context) {

	request, err := getLocateRequestFromBody(ctx)
	if err != nil {
		errResponse := ErrorResponse{http.StatusBadRequest, "Invalid body", err.Error()}
		controller.logger.Info(errResponse.ErrorMsg)
		ctx.JSON(errResponse.StatusCode, errResponse)
		return
	}

	group, err := controller.locateGroup.Invoke(ctx.Request.Context(), request)
	if err != nil {
		if errors.Is(err, groups.GroupNotFoundError) {
			errResponse := ErrorResponse{http.StatusNotFound, "Error finding group", err.Error()}
			controller.logger.Info(errResponse.ErrorMsg)
			ctx.Status(errResponse.StatusCode)
			return
		}

		errResponse := ErrorResponse{http.StatusInternalServerError, "Error locating group", err.Error()}
		controller.logger.Info(errResponse.ErrorMsg)
		ctx.JSON(errResponse.StatusCode, errResponse)
		return
	}

	if group.Car != nil {
		ctx.JSON(http.StatusOK, group.Car)
	} else {
		ctx.JSON(http.StatusOK,group)
	}
}


func getLocateRequestFromBody(ctx *gin.Context) (dto.LocateRequest, error) {
	request := dto.LocateRequest{}
	contentType := ctx.Request.Header.Get("Content-type")
	if contentType == "application/x-www-form-urlencoded" {
		err := ctx.Request.ParseForm()
		if err != nil {
			return dto.LocateRequest{}, err
		}
		groupID := ctx.Request.Form.Get("ID")
		if groupID == "" {
			return dto.LocateRequest{}, errors.New("invalid Request form")
		}
		request.ID, err = strconv.Atoi(groupID)
		if err != nil {
			return dto.LocateRequest{}, err
		}

	} else {
		err := ctx.BindJSON(&request)
		if err != nil {
			return dto.LocateRequest{}, err
		}
	}

	return request, nil
}

