package groups

import (
	"errors"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/dto"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/groups"
	"github.com/gin-gonic/gin"
	"net/http"
)

func (controller *GroupsController) HandleJourney(ctx *gin.Context) {
	request, err := getJourneyRequestFromBody(ctx)
	if err != nil {
		errResponse := ErrorResponse{http.StatusBadRequest, "Invalid body", err.Error()}
		controller.logger.Info(errResponse.ErrorMsg)
		ctx.JSON(errResponse.StatusCode, errResponse)
		return
	}
	err = controller.postJourney.Invoke(ctx.Request.Context(), request)
	if err != nil {
		if errors.Is(err, groups.GroupAlreadyAssigned) {
			errResponse := ErrorResponse{http.StatusConflict, "Error assigning car to group", err.Error()}
			controller.logger.Info(errResponse.ErrorMsg)
			ctx.JSON(errResponse.StatusCode, errResponse)
			return
		}
		errResponse := ErrorResponse{http.StatusInternalServerError, "Error posting journey", err.Error()}
		controller.logger.Info(errResponse.ErrorMsg)
		ctx.JSON(errResponse.StatusCode, errResponse)
		return
	}
	ctx.Status(http.StatusAccepted)
}

func getJourneyRequestFromBody(ctx *gin.Context) (dto.JourneyRequest, error) {
	request := dto.JourneyRequest{}

	err := ctx.BindJSON(&request)
	if err != nil {
		return dto.JourneyRequest{}, err
	}
	return request, nil
}

