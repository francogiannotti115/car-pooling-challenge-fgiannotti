package groups

import (
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/actions"
	"go.uber.org/zap"
)

type GroupsController struct {
	logger       *zap.SugaredLogger
	postJourney  actions.PostJourney
	locateGroup  actions.LocateGroup
	dropoffGroup actions.DropoffGroup
}

type ErrorResponse struct {
	StatusCode int    `json:"status"`
	Message    string `json:"message"`
	ErrorMsg   string `json:"error"`
}

func NewGroupsController(logger *zap.SugaredLogger, postJourney actions.PostJourney, locateGroup actions.LocateGroup, dropoff actions.DropoffGroup) GroupsController {
	return GroupsController{logger, postJourney, locateGroup, dropoff}
}
