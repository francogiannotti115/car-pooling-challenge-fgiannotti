package groups

import (
	"errors"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/dto"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/groups"
	"github.com/gin-gonic/gin"
	"net/http"
)

func (controller *GroupsController) HandleDropoff(ctx *gin.Context) {
	request, err := getDropoffRequestFromBody(ctx)
	if err != nil {
		errResponse := ErrorResponse{http.StatusBadRequest, "Invalid body", err.Error()}
		controller.logger.Info(errResponse.ErrorMsg)
		ctx.JSON(errResponse.StatusCode, errResponse)
		return
	}

	err = controller.dropoffGroup.Invoke(ctx.Request.Context(), request)
	if err != nil {
		if errors.Is(err, groups.GroupNotFoundError) {
			errResponse := ErrorResponse{http.StatusNotFound, "Error finding group", err.Error()}
			controller.logger.Info(errResponse.ErrorMsg)
			ctx.JSON(errResponse.StatusCode, errResponse)
			return
		}

		errResponse := ErrorResponse{http.StatusInternalServerError, "Error dropping group", err.Error()}
		controller.logger.Info(errResponse.ErrorMsg)
		ctx.JSON(errResponse.StatusCode, errResponse)
		return
	}

	ctx.Status(http.StatusNoContent)
}

func getDropoffRequestFromBody(ctx *gin.Context) (dto.DropoffRequest, error) {
	request := dto.DropoffRequest{}
	err := ctx.Request.ParseForm()
	if err != nil {
		return dto.DropoffRequest{}, err
	}
	request.ID = ctx.Request.Form.Get("ID")
	if request.ID == "" {
		return dto.DropoffRequest{}, errors.New("invalid Request form")
	}
	return request, nil
}
