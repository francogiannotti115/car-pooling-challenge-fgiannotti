package controllers

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/dto"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/actions/mock_update_cars"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zaptest"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPUTCarEndpointReturns200(t *testing.T) {
	mockPutCarsReq := dto.PutCarsRequest{
		Cars: []dto.CarDTO{{1, 6}, {1, 2}},
	}
	mockPutCarsReqBytes, _ := json.Marshal(mockPutCarsReq.Cars)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	loggerMock := zaptest.NewLogger(t).Sugar()
	updateCarsMock := mock_update_cars.NewMockUpdateCars(ctrl)
	updateCarsMock.EXPECT().Invoke(context.Background(), mockPutCarsReq)

	router := SetupTestRouter(NewCarsController(loggerMock, updateCarsMock))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPut, "/cars", bytes.NewBuffer(mockPutCarsReqBytes))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.NotNil(t, w.Body)
}

func SetupTestRouter(carsController CarsController) *gin.Engine {
	r := gin.Default()
	r.PUT("/cars", carsController.HandlePutCars)
	r.POST("/users/login", carsController.HandlePutCars)
	r.GET("/users/:username", carsController.HandlePutCars)

	return r
}
