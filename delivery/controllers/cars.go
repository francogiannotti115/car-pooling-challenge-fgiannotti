package controllers

import (
	"fmt"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/dto"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/actions"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
)

type CarsController struct {
	logger          *zap.SugaredLogger
	updateCarAction actions.UpdateCars
}

func NewCarsController(logger *zap.SugaredLogger, updateCarsAction actions.UpdateCars) CarsController {
	return CarsController{logger, updateCarsAction}
}

type ErrorResponse struct {
	StatusCode int    `json:"status"`
	Message    string `json:"message"`
	ErrorMsg   string `json:"error"`
}

func (controller *CarsController) HandlePutCars(ctx *gin.Context) {
	request, err := getPutCarsRequestFromBody(ctx)
	if err != nil {
		errResponse := ErrorResponse{http.StatusBadRequest, "Invalid body", err.Error()}
		controller.logger.Info(errResponse.ErrorMsg)
		ctx.JSON(errResponse.StatusCode, errResponse)
		return
	}
	err = controller.updateCarAction.Invoke(ctx.Request.Context(), request)
	if err != nil {
		errResponse := ErrorResponse{http.StatusBadRequest, "Error executing update car action", err.Error()}
		controller.logger.Info(errResponse.ErrorMsg)
		ctx.JSON(errResponse.StatusCode, errResponse)
		return
	}
	ctx.JSON(http.StatusOK, fmt.Sprintf("Saved %d cars", len(request.Cars)))
}

func getPutCarsRequestFromBody(c *gin.Context) (dto.PutCarsRequest, error) {
	request := dto.PutCarsRequest{}

	err := c.BindJSON(&request.Cars)
	if err != nil {
		return dto.PutCarsRequest{}, err
	}
	return request, nil
}
