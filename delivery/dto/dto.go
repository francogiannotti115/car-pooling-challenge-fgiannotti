package dto

import (
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain"
)

type PutCarsRequest struct {
	Cars []CarDTO
}

type CarDTO struct {
	ID         int `json:"id"`
	TotalSeats int `json:"seats"`
}

func (c *CarDTO) ToDomainCar() domain.Car {
	return domain.Car{ID: c.ID, TotalSeats: c.TotalSeats, RemainingSeats: c.TotalSeats}
}

type JourneyRequest struct {
	ID     int `json:"id"`
	People int `json:"people"`
}

type LocateRequest struct {
	ID int `json,form:"id,string" binding:"required"`
}

type DropoffRequest struct {
	ID string `form:"id"`
}
