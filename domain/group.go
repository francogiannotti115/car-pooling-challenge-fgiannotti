package domain

type Group struct {
	ID           int  `json:"id"`
	PeopleAmount int  `json:"people"`
	Car          *Car `json:"car"`
}
