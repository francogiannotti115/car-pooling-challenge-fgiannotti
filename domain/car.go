package domain

type Car struct {
	ID             int `json:"id"`
	TotalSeats     int `json:"seats"`
	RemainingSeats int `json:"remaining_seats"`
}
