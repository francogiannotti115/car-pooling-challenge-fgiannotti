package cars

import (
	"errors"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain"
)

var CarNotFoundError = errors.New("car not found")
var DuplicatedCar = errors.New("car already exists")

var MaxCarSeats = 6

//go:generate mockgen -destination=./mock_cars/mock_cars.go -source=cars.go
type Repo interface {
	Delete(car domain.Car) error
	Flush() error
	GetAllByRemainingSeats() ([][]domain.Car, error)
	GetAnyWithRemainingSeats(seatsRequired int) (domain.Car, error)
	ReleaseSeats(car domain.Car) error
	Save(car domain.Car) error
	Update(car domain.Car) error
}
