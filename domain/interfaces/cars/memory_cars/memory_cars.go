package memory_cars

import (
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/cars"
	"go.uber.org/zap"
	"sync"
)

type MemoryCarsRepo struct {
	DB       [][]domain.Car
	logger   *zap.SugaredLogger
	cache    map[int]domain.Car
	carsLock *carsLock
}

type carsLock struct {
	allCarsLock    *sync.RWMutex
	singleCarLocks map[int]*sync.Mutex
}

func NewMemoryCarsRepo(logger *zap.SugaredLogger) cars.Repo {
	carsCap := make([][]domain.Car, 7)
	internalCache := make(map[int]domain.Car, 1000000)
	lock := carsLock{allCarsLock: &sync.RWMutex{}, singleCarLocks: make(map[int]*sync.Mutex)}

	return &MemoryCarsRepo{DB: carsCap, cache: internalCache, logger: logger, carsLock: &lock}
}

func (repo *MemoryCarsRepo) Flush() error {
	repo.carsLock.allCarsLock.Lock()
	defer repo.carsLock.allCarsLock.Unlock()

	repo.DB = make([][]domain.Car, 7)
	repo.cache = make(map[int]domain.Car)
	return nil
}

// Save - Always overwrites entry
func (repo *MemoryCarsRepo) Save(car domain.Car) error {
	repo.carsLock.allCarsLock.RLock()
	defer repo.carsLock.allCarsLock.RUnlock()
	mutex := repo.getOrCreateSingleCarLock(car.ID)

	mutex.Lock()
	defer mutex.Unlock()
	_, ok := repo.cache[car.ID]
	if ok {
		return cars.DuplicatedCar
	}
	repo.DB[car.RemainingSeats] = append(repo.DB[car.RemainingSeats], car)
	repo.cache[car.ID] = car
	return nil
}

func (repo *MemoryCarsRepo) Get(carID int) (domain.Car, error) {
	repo.carsLock.allCarsLock.RLock()
	defer repo.carsLock.allCarsLock.RUnlock()
	mutex := repo.getOrCreateSingleCarLock(carID)

	mutex.Lock()
	defer mutex.Unlock()
	carFound, ok := repo.cache[carID]
	if !ok {
		return domain.Car{}, cars.CarNotFoundError
	}
	return carFound, nil
}

func (repo *MemoryCarsRepo) GetAllByRemainingSeats() ([][]domain.Car, error) {
	return repo.DB, nil
}

func (repo *MemoryCarsRepo) Update(car domain.Car) error {
	repo.carsLock.allCarsLock.RLock()
	defer repo.carsLock.allCarsLock.RUnlock()
	mutex := repo.getOrCreateSingleCarLock(car.ID)

	mutex.Lock()
	defer mutex.Unlock()
	carFound, i, j := repo.searchWithFullScan(car.ID)
	if carFound == nil {
		return cars.CarNotFoundError
	}

	repo.DB[i] = remove(repo.DB[i], j)
	repo.DB[car.RemainingSeats] = append(repo.DB[car.RemainingSeats], car)
	repo.cache[car.ID] = car

	return nil
}

func (repo *MemoryCarsRepo) Delete(car domain.Car) error {
	repo.carsLock.allCarsLock.RLock()
	defer repo.carsLock.allCarsLock.RUnlock()
	mutex := repo.getOrCreateSingleCarLock(car.ID)

	mutex.Lock()
	defer mutex.Unlock()
	carFound, i, j := repo.searchWithFullScan(car.ID)
	if carFound == nil {
		return cars.CarNotFoundError
	}
	repo.DB[i] = remove(repo.DB[i], j)
	delete(repo.cache, car.ID)

	return nil
}

func (repo *MemoryCarsRepo) GetAnyWithRemainingSeats(seatsRequired int) (domain.Car, error) {
	repo.carsLock.allCarsLock.RLock()
	defer repo.carsLock.allCarsLock.RUnlock()

	//TODO: check lock?
	if len(repo.DB[seatsRequired]) <= 0 {
		return domain.Car{}, cars.CarNotFoundError
	}
	assignedCar := repo.DB[seatsRequired][0]
	//repo.DB[seatsRequired] = repo.DB[seatsRequired][1:]
	return assignedCar, nil
}

func (repo *MemoryCarsRepo) ReleaseSeats(car domain.Car) error {
	repo.carsLock.allCarsLock.RLock()
	defer repo.carsLock.allCarsLock.RUnlock()
	mutex := repo.getOrCreateSingleCarLock(car.ID)

	mutex.Lock()
	defer mutex.Unlock()
	remainingSeats := car.RemainingSeats

	for i, c := range repo.DB[remainingSeats] {
		if c.ID == car.ID {
			repo.DB[remainingSeats] = remove(repo.DB[remainingSeats], i)

			car.RemainingSeats = car.TotalSeats
			repo.DB[car.RemainingSeats] = append(repo.DB[car.RemainingSeats], car)
			repo.cache[car.ID] = car
		}
	}

	return nil
}

//remove without order
func remove(cars []domain.Car, i int) []domain.Car {
	cars[i] = cars[len(cars)-1]
	return cars[:len(cars)-1]
}

func (repo *MemoryCarsRepo) searchWithFullScan(carID int) (*domain.Car, int, int) {
	for i, carsBySeat := range repo.DB {
		for j, car := range carsBySeat {
			if car.ID == carID {
				return &car, i, j
			}
		}
	}
	return nil, 0, 0
}

func (repo *MemoryCarsRepo) getOrCreateSingleCarLock(carID int) *sync.Mutex {
	mutex, mutexExists := repo.carsLock.singleCarLocks[carID]
	if !mutexExists {
		mutex = &sync.Mutex{}
		repo.carsLock.singleCarLocks[carID] = mutex
	}
	return mutex
}
