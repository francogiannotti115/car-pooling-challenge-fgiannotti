package groups

import (
	"errors"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain"
)

var GroupNotFoundError = errors.New("group not found")
var GroupAlreadyAssigned = errors.New("group already has a car assigned")

//go:generate mockgen -destination=./mock_groups/mock_groups.go -source=groups.go
type Repo interface {
	Save(group domain.Group) error
	Get(groupID int) (domain.Group, error)
	Delete(groupID int) error
}
