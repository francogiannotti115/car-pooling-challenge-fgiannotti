package memory_groups

import (
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zaptest"
	"testing"
)

func TestGetGroupSuccess(t *testing.T) {
	expectedGroup := domain.Group{ID: 1, PeopleAmount: 6, Car: nil}
	groupsRepo := &MemoryGroupsRepo{DB: map[int]domain.Group{1: expectedGroup}}
	car, err := groupsRepo.Get(1)
	assert.Equal(t, car, expectedGroup)
	assert.Nil(t, err)
}

func TestGetGroupNoMatchError(t *testing.T) {
	emptyMap := map[int]domain.Group{}
	carsRepo := &MemoryGroupsRepo{
		DB:     emptyMap,
		logger: zaptest.NewLogger(t).Sugar(),
	}
	_, err := carsRepo.Get(1)
	assert.NotNil(t, err)
}
