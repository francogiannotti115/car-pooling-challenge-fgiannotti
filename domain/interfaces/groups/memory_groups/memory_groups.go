package memory_groups

import (
	"fmt"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/domain/interfaces/groups"
	"go.uber.org/zap"
)

type MemoryGroupsRepo struct {
	DB     map[int]domain.Group
	logger *zap.SugaredLogger
}

func NewMemoryGroupsRepo(logger *zap.SugaredLogger) groups.Repo {
	return &MemoryGroupsRepo{DB: map[int]domain.Group{}, logger: logger}
}

func (l *MemoryGroupsRepo) Flush() error {
	l.DB = make(map[int]domain.Group, 0)
	return nil
}

// Save - Always overwrites entry
func (l *MemoryGroupsRepo) Save(group domain.Group) error {
	l.DB[group.ID] = group
	return nil
}

func (l *MemoryGroupsRepo) Get(groupID int) (domain.Group, error) {
	group, ok := l.DB[groupID]
	if !ok {
		l.logger.Info("Car not found in map")
		return domain.Group{}, fmt.Errorf("%w: groupID %d", groups.GroupNotFoundError, groupID)
	}

	return group, nil
}

func (l *MemoryGroupsRepo) Delete(groupID int) error {
	delete(l.DB, groupID)
	return nil
}
