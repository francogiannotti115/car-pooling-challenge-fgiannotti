package cmd

import (
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/controllers"
	"github.com/fgiannotti/car-pooling-challenge-fgiannotti/delivery/controllers/groups"
	"github.com/gin-gonic/gin"
	"mime"
	"net/http"
	"strings"
)

func SetupRouter(carsController controllers.CarsController, groupsController groups.GroupsController) *gin.Engine {
	r := gin.Default()

	r.GET("/status", HealthCheck)
	addRoute(r, "/cars", http.MethodPut, withContentType([]string{"application/json"}, carsController.HandlePutCars))
	addRoute(r, "/journey", http.MethodPost, withContentType([]string{"application/json"}, groupsController.HandleJourney))
	addRoute(r, "/locate", http.MethodPost, withContentType([]string{"application/json", "application/x-www-form-urlencoded"}, groupsController.HandleLocate))
	addRoute(r, "/dropoff", http.MethodPost, withContentType([]string{"application/x-www-form-urlencoded"}, groupsController.HandleDropoff))

	return r
}

func withContentType(contentTypes []string, handler func(ctx *gin.Context)) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		anyValidContentType := false
		for _, contentType := range contentTypes {
			anyValidContentType = anyValidContentType || hasContentType(ctx.Request, contentType)
		}

		if !anyValidContentType {
			ctx.Status(http.StatusUnsupportedMediaType)
			return
		}
		handler(ctx)
	}
}

func addRoute(r *gin.Engine, route string, requiredMethod string, handler func(ctx *gin.Context)) {

	for _, method := range httpMethods {
		if method != requiredMethod {
			addRouteFromMethod(r, route, method, invalidMethodHandler)
		}
	}
	addRouteFromMethod(r, route, requiredMethod, handler)
}

func addRouteFromMethod(r *gin.Engine, route string, method string, handler func(ctx *gin.Context)) {
	switch method {
	case http.MethodGet:
		r.GET(route, handler)

	case http.MethodHead:
		r.HEAD(route, handler)

	case http.MethodPost:
		r.POST(route, handler)

	case http.MethodPut:
		r.PUT(route, handler)

	case http.MethodPatch:
		r.PATCH(route, handler)

	case http.MethodDelete:
		r.DELETE(route, handler)

	case http.MethodOptions:
		r.OPTIONS(route, handler)

	}
}

var invalidMethodHandler = func(ctx *gin.Context) { ctx.Status(http.StatusMethodNotAllowed) }

var httpMethods = []string{
	http.MethodGet,
	http.MethodHead,
	http.MethodPost,
	http.MethodPut,
	http.MethodPatch,
	http.MethodDelete,
	http.MethodOptions,
}

func HealthCheck(c *gin.Context) {
	c.JSON(200, gin.H{"status": "ok"})
}

// Determine whether the request `content-type` includes a
// server-acceptable mime-type
//
// Failure should yield an HTTP 415 (`http.StatusUnsupportedMediaType`)
func hasContentType(r *http.Request, mimetype string) bool {
	contentType := r.Header.Get("Content-type")
	if contentType == "" {
		return mimetype == "application/octet-stream"
	}

	for _, v := range strings.Split(contentType, ",") {
		t, _, err := mime.ParseMediaType(v)
		if err != nil {
			break
		}
		if t == mimetype {
			return true
		}
	}
	return false
}
